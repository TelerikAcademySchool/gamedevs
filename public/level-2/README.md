> You can see all the examples in the examples folder.

## Portfolio

The porfolio contains all the study examples, projects and workshops, used in Game Development program by Telerik Academy School.  
**The examples are intended for students and teachers of Telerik Academy School.**

Check the examples at https://telerikacademyschool.gitlab.io/gamedevs/.

## Website
Learn more about Telerik Academy School at https://www.telerikacademy.com/school.

## Moodle
If you are already a student, check your course at https://www.school.telerikacademy.com.
