let lampBlue = tryToLoad("lampBlue", "blue"),
    lampYellow = tryToLoad("lampYellow", "yellow"),
    lampGreen = tryToLoad("lampGreen", "green"),
    lampGray = tryToLoad("lampGray", "gray"),
    lampRed = tryToLoad("lampRed", "red"),
    lampPink = tryToLoad("lampPink", "pink"),
    tipNaKletka = [],
    kartinkaZaTip = [lampGray, lampYellow, lampBlue, lampPink, lampRed, lampGreen],
    rejimNaSvetvane = 0;
for (let i = 0; i < 8; i++) {
    tipNaKletka[i] = []
    for (let j = 0; j < 6; j++) {
        tipNaKletka[i][j] = randomInteger(6);
    }
}

function update() {
    // Purvonachalno priemame che vsichki lampi sa ednakvi 
    let vEdinCvqtLiSa = true;
    for (let x = 0; x < 8; x++) {
        for (let y = 0; y < 6; y++) {
            // Ako otkriem razlichna lampa 
            if (tipNaKletka[0][0] != tipNaKletka[x][y]) {
                vEdinCvqtLiSa = false;
            }
        }
    }
    if (vEdinCvqtLiSa) {
        console.log("WIN");
    }
}

function draw() {
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 6; j++) {
            let risuvanTip = tipNaKletka[i][j];
            drawImage(kartinkaZaTip[risuvanTip], i * 100, j * 100, 100, 100);
        }
    }
}

function mouseup() {
    let kliknatKol = Math.floor(mouseX / 100),
        kliknatRed = Math.floor(mouseY / 100);
    if (rejimNaSvetvane == 0) {
        if (kliknatKol >= 0 && kliknatRed >= 0 && kliknatKol < 8 && kliknatRed < 6) {
            tipNaKletka[kliknatKol][kliknatRed] = (tipNaKletka[kliknatKol][kliknatRed] + 1) % 6;
        }
    }

    if (rejimNaSvetvane == 1) {
        k = kliknatKol;
        r = kliknatRed;
        tipNaKletka[k][r] = (tipNaKletka[k][r] + 1) % 6;
        tipNaKletka[k + 1][r] = (tipNaKletka[k + 1][r] + 1) % 6;
        tipNaKletka[k - 1][r] = (tipNaKletka[k - 1][r] + 1) % 6;
        tipNaKletka[k][r + 1] = (tipNaKletka[k][r + 1] + 1) % 6;
        tipNaKletka[k][r - 1] = (tipNaKletka[k][r - 1] + 1) % 6;
    }

    if (rejimNaSvetvane == 2) {
        while (kliknatKol >= 0 && kliknatRed >= 0 && kliknatKol < 8 && kliknatRed < 6) {
            tipNaKletka[kliknatKol][kliknatRed] = (tipNaKletka[kliknatKol][kliknatRed] + 1) % 6;
            kliknatKol--;
        }
    }

    if (rejimNaSvetvane == 3) {
        let r = kliknatRed,
            k = kliknatKol;
        tipNaKletka[k - 1][r - 2] = (tipNaKletka[k - 1][r - 2] + 1) % 6;
        tipNaKletka[k + 1][r - 2] = (tipNaKletka[k + 1][r - 2] + 1) % 6;
        tipNaKletka[k + 1][r + 2] = (tipNaKletka[k + 1][r + 2] + 1) % 6;
        tipNaKletka[k - 1][r + 2] = (tipNaKletka[k - 1][r + 2] + 1) % 6;
        tipNaKletka[k - 2][r - 1] = (tipNaKletka[k - 2][r + 1] + 1) % 6;
        tipNaKletka[k + 2][r + 1] = (tipNaKletka[k + 2][r + 1] + 1) % 6;
        tipNaKletka[k + 2][r - 1] = (tipNaKletka[k + 2][r - 1] + 1) % 6;
        tipNaKletka[k - 2][r + 1] = (tipNaKletka[k - 2][r + 1] + 1) % 6;

    }
}

function keyup(key) {

    //кодът на клавиша 0   // кодът на клавиша 4 

    if (key >= 48 && key < 52) {

        rejimNaSvetvane = key - 48;

    }

}
