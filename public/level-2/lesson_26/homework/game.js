let lampBlue = tryToLoad("lampBlue", "blue"),
    lampYellow = tryToLoad("lampYellow", "yellow"),
    lampGreen = tryToLoad("lampGreen", "green"),
    lampGray = tryToLoad("lampGray", "gray"),
    lampRed = tryToLoad("lampRed", "red"),
    lampPink = tryToLoad("lampPink", "pink"),
    kartinkaZaPobeda = tryToLoad("kartinkaZaPobeda", "white"),
    kartinkaZaZaguba = tryToLoad("kartinkaZaZaguba", "red"),
    tipNaKletka = [],
    kartinkaZaTip = [lampGray, lampYellow, lampBlue, lampPink, lampRed, lampGreen],
    rejimNaSvetvane = 0,
    vEdinCvqtLiSa = false,
    razmerNaKletka = 100,
    brKoloni = 800 / razmerNaKletka,
    brRedove = 600 / razmerNaKletka;
for (let i = 0; i < brKoloni; i++) {
    tipNaKletka[i] = []
    for (let j = 0; j < brRedove; j++) {
        tipNaKletka[i][j] = randomInteger(6);
    }
}

function update() {
    // Purvonachalno priemame che vsichki lampi sa ednakvi 
    vEdinCvqtLiSa = true;
    for (let x = 0; x < brKoloni; x++) {
        for (let y = 0; y < brRedove; y++) {
            // Ako otkriem razlichna lampa 
            if (tipNaKletka[0][0] != tipNaKletka[x][y]) {
                vEdinCvqtLiSa = false;
            }
        }
    }
    if (vEdinCvqtLiSa == true) {
        console.log("WIN");
    }
}

function draw() {
    if (vEdinCvqtLiSa == true && tipNaKletka[0][0] == 0) {
        drawImage(kartinkaZaZaguba, 0, 0, 800, 600);
    }
    if (vEdinCvqtLiSa == true && tipNaKletka[0][0] != 0) {
        drawImage(kartinkaZaPobeda, 0, 0, 800, 600);
    }
    if (vEdinCvqtLiSa == false) {
        for (let i = 0; i < brKoloni; i++) {
            for (let j = 0; j < brRedove; j++) {
                let risuvanTip = tipNaKletka[i][j];
                drawImage(kartinkaZaTip[risuvanTip], i * razmerNaKletka, j * razmerNaKletka, razmerNaKletka, razmerNaKletka);
            }
        }
    }
}

function mouseup() {
    if (vEdinCvqtLiSa == true) {
        razmerNaKletka = razmerNaKletka / 2;
        brKoloni = 800 / razmerNaKletka;
        brRedove = 600 / razmerNaKletka;
        tipNaKletka = [];
        for (let i = 0; i < brKoloni; i++) {
            tipNaKletka[i] = [];
            for (let j = 0; j < brRedove; j++) {
                tipNaKletka[i][j] = randomInteger(6);
            }
        }
    } else {
        let kliknatKol = Math.floor(mouseX / 100),
            kliknatRed = Math.floor(mouseY / 100);
        if (rejimNaSvetvane == 0) {
            if (kliknatKol >= 0 && kliknatRed >= 0 && kliknatKol < brKoloni && kliknatRed < brRedove) {
                tipNaKletka[kliknatKol][kliknatRed] = (tipNaKletka[kliknatKol][kliknatRed] + 1) % 6;
            }
        }

        if (rejimNaSvetvane == 1) {
            k = kliknatKol;
            r = kliknatRed;
            if (k >= 0 && r >= 0 && k < brKoloni && r < brRedove) {
                tipNaKletka[k][r] = (tipNaKletka[k][r] + 1) % 6;
            }
            if (k + 1 >= 0 && r >= 0 && k + 1 < brKoloni && r < brRedove) {
                tipNaKletka[k + 1][r] = (tipNaKletka[k + 1][r] + 1) % 6;
            }
            if (k - 1 >= 0 && r >= 0 && k - 1 < brKoloni && r < brRedove) {
                tipNaKletka[k - 1][r] = (tipNaKletka[k - 1][r] + 1) % 6;
            }
            if (k >= 0 && r + 1 >= 0 && k < brKoloni && r + 1 < brRedove) {
                tipNaKletka[k][r + 1] = (tipNaKletka[k][r + 1] + 1) % 6;
            }
            if (k >= 0 && r - 1 >= 0 && k < brKoloni && r - 1 < brRedove) {
                tipNaKletka[k][r - 1] = (tipNaKletka[k][r - 1] + 1) % 6;
            }
        }

        if (rejimNaSvetvane == 2) {
            while (kliknatKol >= 0 && kliknatRed >= 0 && kliknatKol < brKoloni && kliknatRed < brRedove) {
                tipNaKletka[kliknatKol][kliknatRed] = (tipNaKletka[kliknatKol][kliknatRed] + 1) % 6;
                kliknatKol--;
            }
        }

        if (rejimNaSvetvane == 3) {
            let r = kliknatRed,
                k = kliknatKol;
            if (k - 1 >= 0 && r - 2 >= 0 && k - 1 < brKoloni && r - 2 < brRedove) {
                tipNaKletka[k - 1][r - 2] = (tipNaKletka[k - 1][r - 2] + 1) % 6;
            }
            if (k + 1 >= 0 && r - 2 >= 0 && k + 1 < brKoloni && r - 2 < brRedove) {
                tipNaKletka[k + 1][r - 2] = (tipNaKletka[k + 1][r - 2] + 1) % 6;
            }
            if (k + 1 >= 0 && r + 2 >= 0 && k + 1 < brKoloni && r + 2 < brRedove) {
                tipNaKletka[k + 1][r + 2] = (tipNaKletka[k + 1][r + 2] + 1) % 6;
            }
            if (k - 1 >= 0 && r + 2 >= 0 && k - 1 < brKoloni && r + 2 < brRedove) {
                tipNaKletka[k - 1][r + 2] = (tipNaKletka[k - 1][r + 2] + 1) % 6;
            }
            if (k - 2 >= 0 && r - 1 >= 0 && k - 2 < brKoloni && r - 1 < brRedove) {
                tipNaKletka[k - 2][r - 1] = (tipNaKletka[k - 2][r - 1] + 1) % 6;
            }
            if (k + 2 >= 0 && r + 1 >= 0 && k + 2 < brKoloni && r + 1 < brRedove) {
                tipNaKletka[k + 2][r + 1] = (tipNaKletka[k + 2][r + 1] + 1) % 6;
            }
            if (k + 2 >= 0 && r - 1 >= 0 && k + 2 < brKoloni && r - 1 < brRedove) {
                tipNaKletka[k + 2][r - 1] = (tipNaKletka[k + 2][r - 1] + 1) % 6;
            }
            if (k - 2 >= 0 && r + 1 >= 0 && k - 2 < brKoloni && r + 1 < brRedove) {
                tipNaKletka[k - 2][r + 1] = (tipNaKletka[k - 2][r + 1] + 1) % 6;
            }
        }
    }
}

function keyup(key) {
    //кодът на клавиша 0   // кодът на клавиша 4 
    if (key >= 48 && key < 52) {
        rejimNaSvetvane = key - 48;
    }
}
