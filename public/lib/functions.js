// Redraw will be executed many times
function redraw() {
    context.clearRect(0, 0, canvas.width, canvas.height);

    // Call draw function from game.js
    draw();
    context.globalAlpha = 1;
    context.font = "10px Arial";

    // This will call redraw after some time
    reqAnimationFrame(redraw);
};

function init() {
    if ('ontouchstart' in window || navigator.maxTouchPoints) {
        isMobile = true;
        window.addEventListener("touchstart", function (e) {
            let touchobj = e.changedTouches[0];
            mouseX = parseInt(touchobj.pageX - canvas.offsetLeft);
            mouseY = parseInt(touchobj.pageY - canvas.offsetTop);
            mousedown();
        });
        window.addEventListener("touchend", function (e) {
            let touchobj = e.changedTouches[0];
            mouseX = parseInt(touchobj.pageX - canvas.offsetLeft);
            mouseY = parseInt(touchobj.pageY - canvas.offsetTop);
            mouseup();
        });
        window.addEventListener("touchmove", function (e) {
            let touchobj = e.changedTouches[0];
            mouseX = parseInt(touchobj.pageX - canvas.offsetLeft);
            mouseY = parseInt(touchobj.pageY - canvas.offsetTop);
        });
    }
    window.addEventListener("mousemove", function (e) {
        mouseX = e.pageX - canvas.offsetLeft;
        mouseY = e.pageY - canvas.offsetTop;
    });
    if (typeof mousemove != "undefined") {
        window.addEventListener("mousemove", mousemove);
    }
    if (typeof mouseup != "undefined") {
        window.addEventListener("mouseup", mouseup);
    }
    if (typeof mousedown != "undefined") {
        window.addEventListener("mousedown", mousedown);
    }
    if (typeof keydown != "undefined") {
        window.addEventListener("keydown", function (e) {
            isKeyPressed[e.keyCode] = 1;
            keydown(e.keyCode);
        });
    } else {
        window.addEventListener("keydown", function (e) {
            isKeyPressed[e.keyCode] = 1;
        });
    }
    if (typeof keyup != "undefined") {
        window.addEventListener("keyup", function (e) {
            isKeyPressed[e.keyCode] = 0;
            keyup(e.keyCode);
        });
    } else {
        window.addEventListener("keyup", function (e) {
            isKeyPressed[e.keyCode] = 0;
        });
    }
    if (typeof draw == "undefined") {
        redraw = function () {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.globalAlpha = 1;
            context.fillStyle = "#FF0000";
            context.font = "20px Arial";
            context.fillText("Press <F12> for error info!", 40, 40);
        };
    }
    redraw();
    setInterval(update, updateTime);
}